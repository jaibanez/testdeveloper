import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { employeService } from 'src/services/employe.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public modalRef: BsModalRef;
  bodyResponse: any;
  submitted = false;
  modelForm: FormGroup;
  model = null;
  flagEdit: Boolean;
  boss : any;

  config = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(
    private bsModalService: BsModalService,
    private service: employeService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder
  ) {

  }

  ngOnInit(): void {
    this.bodyResponse = new Array();
    this.getList();
    this.setForms();
  }

  setForms() {
    this.modelForm = this.formBuilder.group({
      id:[''],
      name: ['', Validators.required],
      email: ['', Validators.required],
      funcion: ['', Validators.required],
      phone: ['', Validators.required],
      boss: ['',Validators.required]
    })
  }

  get f() { return this.modelForm.controls; }

  setModelData(response) {
      
      this.f.id.setValue(response.id),
      this.f.name.setValue(response.fullname),
      this.f.email.setValue(response.email),
      this.f.phone.setValue(response.phone),
      this.f.funcion.setValue(response.function)
      
  }

  list(template: TemplateRef<any>) {
    this.openModal(template);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.bsModalService.show(
      template,
      Object.assign({}, this.config, { class: 'gray modal-lg' })
    );
  }

  getList() {
    this.service.getAll().subscribe(
      success => {
        this.bodyResponse = success;
        console.log(this.bodyResponse)
        if (this.bodyResponse == "") {
          this.bodyResponse = null;
          this.toastr.success('Lista vacia', 'Operacion exitosa')
        }
      },
      err => {
        this.toastr.error('Error al obtener lista de empleados', 'Operacion Fallida')
      }
    )
  }

  doHide() {
    this.bsModalService.hide();
    this.modelForm.reset();
  }

  create(template: TemplateRef<any>, item) {
    this.openModal(template);
    if(item != null){
      this.setModelData(item);
      this.flagEdit = true;
      this.boss = item.boss;
      console.log(this.boss)
    }
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.modelForm.invalid) {
      this.toastr.error("Campos obligatorios", "Operacion Fallida");
      return;
    }

    if(!this.flagEdit){
      this.save();
    }else{
      this.edit();
    }
  }

  save(){
    const data ={
      fullname: this.f.name.value,
      email: this.f.email.value,
      function: this.f.funcion.value,
      phone: this.f.phone.value,
      boss: this.f.boss.value
    };

    console.log(data)
    this.service.save(data).subscribe(
      success =>{
        this.getList();
        this.doHide();
        this.toastr.success('Agregado Exitosamente','Operacion Exitosa')
      },
      err=>{
        console.log(err)
        this.toastr.error(err.error.message);
      }
    )
  }

  edit(){
    const data ={
      id: this.f.id.value,
      fullname: this.f.name.value,
      email: this.f.email.value,
      function: this.f.funcion.value,
      phone: this.f.phone.value,
      boss: this.f.boss.value
    }

    console.log(data)
    this.service.update(data).subscribe(
      success =>{
        this.getList();
        this.doHide();
        this.toastr.success('Actualizado Exitosamente','Operacion Exitosa')
      },
      err=>{
        this.toastr.error(err.error.message);
      }
    )
  }

}



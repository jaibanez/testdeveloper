import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class employeService{
    constructor(private http: HttpClient) { }

    getAll(){
        return this.http.get(`${environment.apiUrl}/employe/`);
    }

    save(data){
        return this.http.post(`${environment.apiUrl}/employe/`, data);
    }

    update(data){
        return this.http.put(`${environment.apiUrl}/employe/`, data);
    }
}